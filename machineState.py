from Memory import Memory

class MachineState():

    def __repr__(self):

        return """
iar: {iar}\t\t r0: {r0} 
    \t\t r1: {r1}
op0: {op0}\t\t r2: {r2}
op1: {op1}\t\t r3: {r3}
op2: {op2}\t\t r4: {r4}
op3: {op3}\t\t r5: {r5}

res: {res}\t

program:

{prog}

mem: {mem}
""".format(
            **{
                "iar": self.iar,

                "op0": self.op0,
                "op1": self.op1,
                "op2": self.op2,
                "op3": self.op3,

                "r0": self.r0,
                "r1": self.r1,
                "r2": self.r2,
                "r3": self.r3,
                "r4": self.r4,
                "r5": self.r5,

                "res": self.res,
                "mem": self.mem,
                "prog": self.getProgramDump()
            }
        )

    def getProgramDump(self):

        result = ""

        for i in range(len(self.operations)):

            result += "> " if i == self.iar else "  "
            result += f'{i:4d}' 
            result += ": " + self.operations[i] + "\n"

        return result

    def __init__(self, memsize, operations):

        self.operations = operations

        self.halted = False

        self.iar = 0

        self.op0 = 0
        self.op1 = 0
        self.op2 = 0
        self.op3 = 0

        self.res = 0

        self.r0 = 0
        self.r1 = 0
        self.r2 = 0
        self.r3 = 0
        self.r4 = 0
        self.r5 = 0

        self.mem = Memory(memsize)

    def store(self, dest, orig):

        if (orig not in ["r0","r1","r2","r3","r4","r5", "res"]):

            raise Exception("Invalid register!")

        data = eval("self." + orig) 
        self.mem.setPosition(dest, data)

    def storer(self, destR, orig):

        if (orig not in ["r0","r1","r2","r3","r4","r5", "res"]):

            raise Exception("Invalid register!")

        if (destR not in ["r0","r1","r2","r3","r4","r5", "op0", "op1", "op2", "op3"]):

            raise Exception("Invalid register!")

        data = eval("self." + orig) 
        dest = eval("self." + destR)
        self.mem.setPosition(dest, data)

    def load(self, dest, orig):

        if (dest not in ["r0","r1","r2","r3","r4","r5", "op0", "op1", "op2", "op3"]):

            raise Exception("Invalid register!")

        data = self.mem.getPosition(orig)
        exec("self." + dest + " = data") 

    def loadr(self, dest, origR):

        if (dest not in ["r0","r1","r2","r3","r4","r5", "op0", "op1", "op2", "op3"]):

            raise Exception("Invalid register!")

        if (origR not in ["r0","r1","r2","r3","r4","r5", "res"]):

            raise Exception("Invalid register!")

        orig = eval("self." + origR)
        data = self.mem.getPosition(orig)
        exec("self." + dest + " = data") 

    def loadConst(self, dest, data):

        if (dest not in ["r0","r1","r2","r3","r4","r5", "op0", "op1", "op2", "op3"]):

            raise Exception("Invalid register!")

        exec("self." + dest + " = data") 

    def move(self, dest, orig):

        if (dest not in ["r0","r1","r2","r3","r4","r5", "op0", "op1", "op2", "op3"]):

            raise Exception("Invalid register!")
        
        if (orig not in ["r0","r1","r2","r3","r4","r5", "res"]):

            raise Exception("Invalid register!")

        exec("self." + dest + " = " + "self." + orig)



        