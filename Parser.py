def replaceLabels(string, labels):

    for key in labels:

        string = string.replace(key, str(labels[key]))

    return string

def strip(string):

    while string[0] == " ":

        string = string[1:]

    while string[-1] == " ":

        string = string[:-1]

    return string

class Parser():

    def parse(self, path):

        with open(path, "r") as f:

            self.originalText = f.read()

        lines = self.originalText.split("\n")
        lines = filter((lambda line : len(line) > 0 and not line.isspace()), lines) # filter blank lines
        lines = list(map((lambda line : strip(line)), lines)) # strip border whitespace
        lines = list(zip([x for x in range(len(lines))], lines)) # zip with line number
        labels = {}
        for line in lines:

            linenum = line[0]
            text = line[1]
            if text[-1] == ":":

                if (text[:-1] in labels.keys()):

                    raise Exception("Label already defined")
                
                labels[text[:-1]] = linenum

        lines = list(map((lambda line: line[1]) , lines)) 
        lines = list(map((lambda line: replaceLabels(line, labels)) , lines))
        return lines