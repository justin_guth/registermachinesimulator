from machineState import MachineState
from Parser import Parser

import sys

from instructions.add import Add
from instructions.default import Default
from instructions.div import Div
from instructions.jez import Jez
from instructions.jgz import Jgz
from instructions.jlz import Jlz
from instructions.jmp import Jmp
from instructions.load import Load
from instructions.mov import Mov
from instructions.nop import Nop
from instructions.store import Store
from instructions.sub import Sub
from instructions.label import Label
from instructions.const import Const
from instructions.loadr import LoadR
from instructions.storer import StoreR
from instructions.halt import Halt
from instructions.comment import Comment

operations = [
    Const(),
    Comment(),
    LoadR(),
    StoreR(),
    Add(),
    Div(),
    Jez(),
    Jgz(),
    Jlz(),
    Jmp(),
    Load(),
    Mov(),
    Nop(),
    Store(),
    Sub(),
    Label(),
    Halt(),
    Default()
]


class RegisterMachine():
    def __init__(self, useSteps=True, printState=True):

        self.parser = Parser()
        self.useSteps = useSteps
        self.printState = printState
        self.skip = 0

    def runFile(self, path):

        instructionsText = self.parser.parse(path)

        if instructionsText[-1] != "HALT":
            instructionsText.append("HALT")

        self.runInstructions(instructionsText)

    def runInstructions(self, instructions):

        self.state = MachineState(32, instructions)

        while not self.state.halted:

            if self.skip > 0:

                self.skip -= 1

            if self.printState and self.skip == 0:

                self.printStateDump()

            currentInstruction = instructions[self.state.iar]
            self.state.iar += 1

            for operation in operations:

                if operation.fits(currentInstruction):

                    break

            self.state = operation.tryRun(currentInstruction, self.state)

            if self.useSteps and self.skip == 0:

                x = input()

                if x[:5] == "skip ":

                    self.skip = int(x[5:])

                if x == "halt":

                    exit()

        print("Machine halted!")
        self.printStateDump()

    def printStateDump(self):

        print("=" * 60)
        print("Current state:")
        print(self.state)
        print("=" * 60)


if __name__ == "__main__":

    path = sys.argv[1]
    rm = RegisterMachine()
    rm.runFile(path)
