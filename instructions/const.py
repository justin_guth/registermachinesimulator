class Const():

    def __init__(self):

        pass

    def fits(self, line):

        return line[:5] == "CONST"

    def tryRun(self, line, machineState):

        if (not self.fits(line)):

            raise Exception("Operation invalid!")

        lineSplit = line.split(" ")
        destinationRegister = lineSplit[1]
        constdata = int(lineSplit[2])

        machineState.loadConst(destinationRegister, constdata)

        return machineState