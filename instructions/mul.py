class Mul():

    def __init__(self):

        pass

    def fits(self, line):

        return line[:3] == "MUL"

    def tryRun(self, line, machineState):

        if (not self.fits(line)):

            raise Exception("Operation invalid!")

        operandA = machineState.op0
        operandB = machineState.op1

        result = operandA * operandB

        machineState.res = result

        return machineState