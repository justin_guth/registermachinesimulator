class Jgz():

    def __init__(self):

        pass

    def fits(self, line):

        return line[:3] == "JGZ"

    def tryRun(self, line, machineState):

        if (not self.fits(line)):

            raise Exception("Operation invalid!")

        lineSplit = line.split(" ")
        operandA = machineState.op0

        if (operandA > 0):

            machineState.iar = int(lineSplit[1])

        return machineState