class Halt():

    def __init__(self):

        pass

    def fits(self, line):

        return line[:4] == "HALT"

    def tryRun(self, line, machineState):

        machineState.halted = True
        return machineState