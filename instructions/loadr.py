class LoadR():

    def __init__(self):

        pass

    def fits(self, line):

        return line[:6] == "LOADR "

    def tryRun(self, line, machineState):

        if (not self.fits(line)):

            raise Exception("Operation invalid!")

        lineSplit = line.split(" ")
        destinationRegister = lineSplit[1]
        originAddressRegister = lineSplit[2]

        machineState.loadr(destinationRegister, originAddressRegister)

        return machineState