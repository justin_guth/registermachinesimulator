class Load():

    def __init__(self):

        pass

    def fits(self, line):

        return line[:5] == "LOAD "

    def tryRun(self, line, machineState):

        if (not self.fits(line)):

            raise Exception("Operation invalid!")

        lineSplit = line.split(" ")
        destinationRegister = lineSplit[1]
        originAddress = int(lineSplit[2])

        machineState.load(destinationRegister, originAddress)

        return machineState