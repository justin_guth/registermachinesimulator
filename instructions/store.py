class Store():

    def __init__(self):

        pass

    def fits(self, line):

        return line[:6] == "STORE "

    def tryRun(self, line, machineState):

        if (not self.fits(line)):

            raise Exception("Operation invalid!")

        lineSplit = line.split(" ")
        destinationAddress = int(lineSplit[1])
        originRegister = lineSplit[2]

        machineState.store(destinationAddress, originRegister)

        return machineState