class StoreR():

    def __init__(self):

        pass

    def fits(self, line):

        return line[:7] == "STORER "

    def tryRun(self, line, machineState):

        if (not self.fits(line)):

            raise Exception("Operation invalid!")

        lineSplit = line.split(" ")
        destinationAddressRegister = lineSplit[1]
        originRegister = lineSplit[2]


        machineState.storer(destinationAddressRegister, originRegister)

        return machineState