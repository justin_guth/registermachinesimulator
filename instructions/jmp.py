class Jmp():

    def __init__(self):

        pass

    def fits(self, line):

        return line[:3] == "JMP"

    def tryRun(self, line, machineState):

        if (not self.fits(line)):

            raise Exception("Operation invalid!")

        lineSplit = line.split(" ")

        machineState.iar = int(lineSplit[1])

        return machineState