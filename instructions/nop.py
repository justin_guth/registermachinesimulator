class Nop():

    def __init__(self):

        pass

    def fits(self, line):

        return line[:3] == "NOP"

    def tryRun(self, line, machineState):

        return machineState