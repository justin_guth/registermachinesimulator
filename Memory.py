class Memory():

    def __repr__(self):

        return str(self.memory)

    def __init__(self, size):

        self.size = size
        self.memory = [0] * size
    
    def getPosition(self, index):

        if index >= self.size:

            raise Exception('Invalid memory access! {max} < {index}'.format(max=self.size, index=index))
        
        return self.memory[index]

    def setPosition(self, index, value):

        if index >= self.size:

            raise Exception('Invalid memory access!')

        self.memory[index] = value

    